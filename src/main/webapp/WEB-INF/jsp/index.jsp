<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">

    <head>

    </head>

    <body class="bg-default">
        <form action="login" method="post">
            <div class="form-group mb-3" style="color: red">
                ${fail}
            </div>

            <input class="form-control" placeholder="Username" type="text" name="username">
            <input class="form-control" placeholder="Password" type="password" name="password">
            <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
            <label class="custom-control-label" for=" customCheckLogin">
                <span class="text-muted">Remember me</span>
            </label>
            <div class="text-center">
                <button type="submit" class="btn btn-primary my-4">Sign in</button>
            </div>
        </form>
    </body>

</html>