<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

    <head>


    </head>

    <body class="">
        <h3>Navigation</h3>
        <ul>
            <spring:url value="/dashboard" var="dashboardUrl"></spring:url>
            <spring:url var="skillUrl" value="/skill/categoryList"></spring:url>
            <spring:url var="ivrUrl" value="/ivr/list"></spring:url>
            <li><a href="${dashboardUrl}">Dashboard</a></li>
            <li><a href="${skillUrl}">Skill</a></li>
            <li><a href="${ivrUrl}">IVR and Queue</a></li>
        </ul>
        
        <ul>
            <li>
                ${IsInboundActive}
            </li> 
            <li>
                ${isEnabledExtension}
            </li>
            <li>
                ${callFilterMode}
            </li>
            <li>
                ${dialExtensionKey}
            </li>
            <li>
                ${IsNightMode}
            </li>
            <li>
                ${forwardCallTo}
            </li>
        </ul>

        <br/><br/><hr/><br/><br/>


        <h2>IVR Name : ${ivrModel.name} </h2>

        <br/><br/><hr/><br/><br/>

        <h3>Enable IVR</h3>
        <input type="radio" name="enableIVR" value="on"> On<br/>
        <input type="radio" name="enableIVR" value="off" checked="checked"> Off
        <br/><br/><hr/><br/><br/>


        <h3>Inbound IVR Number</h3>
        <span>Current Number</span>
        <input type="text" name="number"> <input type="button" value="Register" />

        <br/><br/><hr/><br/><br/>

        <h3>Welcome Message:</h3>
        <span>Current File</span>
        <input type="file" name="welcome"> <input type="button" value="upload" />

        <br/><br/><hr/><br/><br/>

        <h3>Music on Hold</h3>
        <span>Current File</span>
        <input type="file" name="hold"> <input type="button" value="upload" />

        <br/><br/><hr/><br/><br/>

        <h3>Call Filter</h3>
        <input type="radio" name="mode" id="offList" value="off" checked=""><label for="offList">Off</label><br/>
        <input type="radio" name="mode" id="blackList" value="blacklist"><label for="blackList">Black List</label><br/>
        <input type="radio" name="mode" id="whiteList" value="whitelist"><label for="whiteList">White List</label><br/>
        <span>Current File</span>
        <input type="file" name="hold"> <input type="button" value="upload" />

        <br/><br/><hr/><br/><br/>   
        <h2>Office Hour</h2>


        Time to Start   : <select style="width: auto;" id="startHour">
            <option value="">HH</option>
            <c:forEach var="i" begin="0" end="23">
                <c:choose>
                    <c:when test="${i<'10'}">
                        <option value="0${i}">0${i}</option>
                    </c:when>    
                    <c:otherwise>
                        <option value="${i}">${i}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <select>
            <option value="">MM</option>
            <c:forEach var="i" begin="0" end="59">
                <c:choose>
                    <c:when test="${i<'10'}">
                        <option value="0${i}">0${i}</option>
                    </c:when>    
                    <c:otherwise>
                        <option value="${i}">${i}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>

        <br/>
        Time to Finish  :<select style="width: auto;" id="startHour">
            <option value="">HH</option>
            <c:forEach var="i" begin="0" end="23">
                <c:choose>
                    <c:when test="${i<'10'}">
                        <option value="0${i}">0${i}</option>
                    </c:when>    
                    <c:otherwise>
                        <option value="${i}">${i}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <select>
            <option value="">MM</option>
            <c:forEach var="i" begin="0" end="59">
                <c:choose>
                    <c:when test="${i<'10'}">
                        <option value="0${i}">0${i}</option>
                    </c:when>    
                    <c:otherwise>
                        <option value="${i}">${i}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select><br/>
        Week Day Start  :<select>
            <option value="" selected="">Day</option>
            <option value="2">Monday</option>
            <option value="3">Tuesday</option>
            <option value="4">Wednesday</option>
            <option value="5">Thursday</option>
            <option value="6">Friday</option>
            <option value="7">Saturday</option>
            <option value="1">Sunday</option>
        </select><br/>
        Week Day Finish :<select>
            <option value="" selected="">Day</option>
            <option value="2">Monday</option>
            <option value="3">Tuesday</option>
            <option value="4">Wednesday</option>
            <option value="5">Thursday</option>
            <option value="6">Friday</option>
            <option value="7">Saturday</option>
            <option value="1">Sunday</option>
        </select><br/>

        Destination if Matches              :   <select >

            <option value="IVR">IVR</option>
            <option value="Voice Mail">Voice Mail</option>
            <option value="Terminate Call">Terminate Call</option>
            <option value="Night-Mode" >Night-Mode</option>
        </select><br/>
        Destination If Time Does Not Matches:   <select>

            <option value="IVR">IVR</option>
            <option value="Voice Mail">Voice Mail</option>
            <option value="Terminate Call">Terminate Call</option>
            <option value="Night-Mode" selected="">Night-Mode</option>
        </select><br/>


    </body>

</html>