<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

    <head>

    </head>

    <body class="">
        <h3>Navigation</h3>
        <ul>
            <spring:url value="/dashboard" var="dashboardUrl"></spring:url>
            <spring:url var="skillUrl" value="/skill/categoryList"></spring:url>
            <spring:url var="ivrUrl" value="/ivr/list"></spring:url>
            <li><a href="${dashboardUrl}">Dashboard</a></li>
            <li><a href="${skillUrl}">Skill</a></li>
            <li><a href="${ivrUrl}">IVR and Queue</a></li>
        </ul>

        <br/><br/><hr/><br/><br/>

        <spring:url var="addIvrUrl" value="/ivr/add"/>
        <a href="${addIvrUrl}">Create New Ivr</a><br/>
        <h3>IVR List</h3>
        <table border="1">
            <thead class="thead-light">
                <tr>
                    <th scope="col">IVR ID</th>
                    <th scope="col">IVR Name</th>
                    <th scope="col">Created on</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <!--
                Iteration of skill categories
                -->
                <c:choose>
                    <c:when test="${empty ivrList}">
                        <tr>
                            <th colspan="4">
                                No Ivr is created
                            </th>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${ivrList}" var="ivr">
                            <tr>
                                <th>${ivr.id}</th>
                                <th>${ivr.name}</th>
                                <th>${ivr.createdOn}</th>
                                <td>
                                    <spring:url var="updateIvrUrl" value="/ivr/update/${ivr.id}" />
                                    <spring:url var="deleteIvrUrl" value="/ivr/delete/${ivr.id}" />
                                    <a href="${updateIvrUrl}">Update</a> / <a href="${deleteIvrUrl}">Delete</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>

    </body>

</html>