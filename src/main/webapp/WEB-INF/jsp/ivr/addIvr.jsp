<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

    <head>

    </head>

    <body class="">
        <h3>Navigation</h3>
        <ul>
            <spring:url value="/dashboard" var="dashboardUrl"></spring:url>
            <spring:url var="skillUrl" value="/skill/categoryList"></spring:url>
            <spring:url var="ivrUrl" value="/ivr/list"></spring:url>
            <li><a href="${dashboardUrl}">Dashboard</a></li>
            <li><a href="${skillUrl}">Skill</a></li>
            <li><a href="${ivrUrl}">IVR and Queue</a></li>
        </ul>

        <br/><br/><hr/><br/><br/>
        
        <h3>Add New IVR</h3>
        <spring:url var="saveUrl" value="/ivr/save" />
        <form:form action="${saveUrl}" method="POST" modelAttribute="ivrForm">
            <form:hidden path="id"/>
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <form:input path="name" class="form-control" placeholder="Enter Ivr name" ></form:input>
                        <!--<input class="form-control" placeholder="Enter Skill name" type="text" name="name">-->
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary my-4">Submit</button>
                </div>
        </form:form>

    </body>

</html>