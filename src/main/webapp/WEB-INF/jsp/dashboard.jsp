<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">

    <head>
    </head>

    <body>
        <h3>Navigation</h3>
        <ul>
            <spring:url value="/dashboard" var="dashboardUrl"></spring:url>
            <spring:url var="skillUrl" value="/skill/categoryList"></spring:url>
            <spring:url var="ivrUrl" value="/ivr/list"></spring:url>
            <li><a href="${dashboardUrl}">Dashboard</a></li>
            <li><a href="${skillUrl}">Skill</a></li>
            <li><a href="${ivrUrl}">IVR and Queue</a></li>
        </ul>
    </body>
</html>