<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

    <head>

    </head>

    <body>
        <h3>Navigation</h3>
        <ul>
            <spring:url value="/dashboard" var="dashboardUrl"></spring:url>
            <spring:url var="skillUrl" value="/skill/categoryList"></spring:url>
            <spring:url var="ivrUrl" value="/ivr/list"></spring:url>
            <li><a href="${dashboardUrl}">Dashboard</a></li>
            <li><a href="${skillUrl}">Skill</a></li>
            <li><a href="${ivrUrl}">IVR and Queue</a></li>
        </ul>


        <br/><br/><hr/><br/><br/>

        <h2>Skill Categories</h2>

        <div>
            <spring:url var="addCatUrl" value="/skill/category/add"/>
            <a href="${addCatUrl}">Create New Category</a>
        </div>


        <table>
            <thead>
                <tr>
                    <th scope="col">Category ID</th>
                    <th scope="col">Category Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <!--
                Iteration of skill categories
                -->
                <c:forEach items="${categories}" var="skillCat">
                    <tr>
                        <th>${skillCat.id}</th>
                        <td>${skillCat.name}</td>
                        <td>
                            <spring:url var="updateCatlUrl" value="/skill/category/update/${skillCat.id}" />
                            <spring:url var="deleteCatUrl" value="/skill/category/delete/${skillCat.id}" />
                            <a href="${updateCatlUrl}">Update</a> / <a href="${deleteCatUrl}">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <br/><br/><hr/><br/><br/>

        <h2>Skills</h2>
        <div>
            <spring:url var="addSkill" value="/skill/add"/>
            <a href="${addSkill}">Create New Skill</a>
        </div>


        <table>
            <thead>
                <tr>
                    <th scope="col">Skill Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Categories</th>
                    <th scope="col" colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${skills}" var="skill">
                    <tr>
                        <th>${skill.name}</th>
                        <td>${skill.description}</td>
                        <td>${skill.skillCategoryID}</td>
                        <td>
                            <spring:url var="updateSkillUrl" value="/skill/update/${skill.id}" />
                            <spring:url var="deleteSkillUrl" value="/skill/delete/${skill.id}" />
                            <a href="${updateSkillUrl}">Update</a> / <a href="${deleteSkillUrl}">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

    </body>

</html>