<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

    <head>

    </head>

    <body class="">
        <h3>Navigation</h3>
        <ul>
            <spring:url value="/dashboard" var="dashboardUrl"></spring:url>
            <spring:url var="skillUrl" value="/skill/categoryList"></spring:url>
            <spring:url var="ivrUrl" value="/ivr/list"></spring:url>
            <li><a href="${dashboardUrl}">Dashboard</a></li>
            <li><a href="${skillUrl}">Skill</a></li>
            <li><a href="${ivrUrl}">IVR and Queue</a></li>
        </ul>
        
        <br/><br/><hr/><br/><br/>
        
        <div>
            <large>Adding Skill Category</large>
        </div>
        <spring:url var="saveUrl" value="/skill/category/save" />
        <form:form action="${saveUrl}" method="post" modelAttribute="categoryForm">  
            <form:hidden path="id"/>
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <form:input path="name" class="form-control" placeholder="Enter category name"/>
                    <!--                                                    <input class="form-control" placeholder="Enter category name" type="text" name="name" value="">-->
                </div>
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary my-4">Submit</button>
            </div>
        </form:form>
    </body>

</html>