<%@taglib  prefix="spring" uri="http://www.springframework.org/tags"%>

    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="${dashboardUrl}">
        <img src="${imgBrand}/blue.png" class="navbar-brand-img" alt="...">
    </a>
    <!-- User -->
    <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
            <a href="#!" class="dropdown-item">
                <i class="ni ni-button-power"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                <div class=" dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <a href="./examples/profile.html" class="dropdown-item">
                    <i class="ni ni-single-02"></i>
                    <span>My profile</span>
                </a>
                <a href="./examples/profile.html" class="dropdown-item">
                    <i class="ni ni-settings-gear-65"></i>
                    <span>Settings</span>
                </a>
                <a href="./examples/profile.html" class="dropdown-item">
                    <i class="ni ni-calendar-grid-58"></i>
                    <span>Activity</span>
                </a>
                <a href="./examples/profile.html" class="dropdown-item">
                    <i class="ni ni-support-16"></i>
                    <span>Support</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#!" class="dropdown-item">
                    <i class="ni ni-user-run"></i>
                    <span>Logout</span>
                </a>
            </div>
        </li>
    </ul>
    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
            <div class="row">
                <div class="col-6 collapse-brand">
                    <a href="${dashboard}">
                        <img src="${imgBrand}/blue.png">
                    </a>
                </div>
                <div class="col-6 collapse-close">
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>
        </div>
        <!-- Form -->

        <!-- Navigation -->
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class=" nav-link active " href="${dashboardUrl}"> <i class="ni ni-tv-2 text-primary"></i> Dashboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="${skillUrl}">
                    <i class="ni ni-planet text-blue"></i> Skills
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="./examples/maps.html">
                    <i class="ni ni-circle-08 text-orange"></i> Users
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="${ivrUrl}">
                    <i class="ni ni-notification-70 text-yellow"></i> IVR & Queue
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="./examples/tables.html">
                    <i class="ni ni-paper-diploma text-red"></i> Reports
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./examples/login.html">
                    <i class="ni ni-settings text-info"></i> Setting
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./examples/register.html">
                    <i class="ni ni-key-25 text-pink"></i> License
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./examples/register.html">
                    <i class="ni ni-money-coins text-blue"></i> Queue Management
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./examples/register.html">
                    <i class="ni ni-glasses-2 text-warning"></i> Agent Monitoring
                </a>
            </li>
        </ul>
    </div>
</div>