/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jaijeet
 */
@Entity
@Table(name = "ivr")
public class IVR
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    private Date createdOn;
    @Column
    private int createdBy;
    @Column
    private int providerID;
    @Column
    private int phoneNumber;
    @Column
    private int isDefault;
    @Column
    private int isDeleted;
    @Column
    private String phonePinNumber;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getCreatedOn()
    {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public int getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(int createdBy)
    {
        this.createdBy = createdBy;
    }

    public int getProviderID()
    {
        return providerID;
    }

    public void setProviderID(int providerID)
    {
        this.providerID = providerID;
    }

    public int getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public int getIsDefault()
    {
        return isDefault;
    }

    public void setIsDefault(int isDefault)
    {
        this.isDefault = isDefault;
    }

    public int getIsDeleted()
    {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public String getPhonePinNumber()
    {
        return phonePinNumber;
    }

    public void setPhonePinNumber(String phonePinNumber)
    {
        this.phonePinNumber = phonePinNumber;
    }

}
