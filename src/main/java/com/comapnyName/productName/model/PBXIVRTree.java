/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jaijeet
 */
@Entity
@Table(name = "PBXIVRTree")
public class PBXIVRTree
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private int ivrID;

    @Column(name = "NodeID")
    private int nodeID;

    @Column(name = "NodeType")
    private String nodeType;

    @Column(name = "KeyPress")
    private int keyPress;

    @Column(name = "Name")
    private String name;

    @Column(name = "ParentNodeID")
    private String parentNodeID;

    @Column(name = "PromptFile")
    private String promptFile;

    @Column(name = "QID")
    private int queueID;
    
    @Column(name = "promptStatus")
    private int promptStatus;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getIvrID()
    {
        return ivrID;
    }

    public void setIvrID(int ivrID)
    {
        this.ivrID = ivrID;
    }

    public int getNodeID()
    {
        return nodeID;
    }

    public void setNodeID(int nodeID)
    {
        this.nodeID = nodeID;
    }

    public String getNodeType()
    {
        return nodeType;
    }

    public void setNodeType(String nodeType)
    {
        this.nodeType = nodeType;
    }

    public int getKeyPress()
    {
        return keyPress;
    }

    public void setKeyPress(int keyPress)
    {
        this.keyPress = keyPress;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getParentNodeID()
    {
        return parentNodeID;
    }

    public void setParentNodeID(String parentNodeID)
    {
        this.parentNodeID = parentNodeID;
    }

    public String getPromptFile()
    {
        return promptFile;
    }

    public void setPromptFile(String promptFile)
    {
        this.promptFile = promptFile;
    }

    public int getQueueID()
    {
        return queueID;
    }

    public void setQueueID(int queueID)
    {
        this.queueID = queueID;
    }

    public int getPromptStatus()
    {
        return promptStatus;
    }

    public void setPromptStatus(int promptStatus)
    {
        this.promptStatus = promptStatus;
    }
}
