/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author jaijeet
 */

@Controller
@RequestMapping(value = "queue")
public class QueueController
{
    public ModelAndView addQueue()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("/queue/add");
        return model;
    }
}
