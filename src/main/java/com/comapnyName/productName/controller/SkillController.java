/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.controller;

import com.comapnyName.productName.model.Skill;
import com.comapnyName.productName.model.SkillCategory;
import com.comapnyName.productName.service.SkillCategoryService;
import com.comapnyName.productName.service.SkillService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author jaijeet
 */

@Controller
@RequestMapping(value = "/skill")
public class SkillController
{
    @Autowired
    private SkillService skillService;
    
    @Autowired
    private SkillCategoryService skillCategoryService;
    
    @RequestMapping(value = "/categoryList", method = RequestMethod.GET)
    public ModelAndView listSkillCategories()
    {
        ModelAndView model = new ModelAndView("/skill/categories");
        List<SkillCategory> categorys = skillCategoryService.listAllSkillCategories();
        List<Skill> skills  = skillService.listAllSkills();
        model.addObject("categories", categorys);
        model.addObject("skills", skills);
        return model;
    }
    
    @RequestMapping(value="/category/add" , method = RequestMethod.GET)
    public ModelAndView addCategory()
    {
        ModelAndView model = new ModelAndView("/skill/catForm");
        SkillCategory category = new SkillCategory();
        model.addObject("categoryForm", category);
        return model;
    }
    
    
    @RequestMapping(value="/category/update/{id}",method = RequestMethod.GET)
    public ModelAndView updateCategory(@PathVariable(value = "id") int id)
    {
        ModelAndView model = new ModelAndView("/skill/catForm");
        SkillCategory category = skillCategoryService.getSkillCategoryByID(id);
        model.addObject("categoryForm", category);                                                                                                                 
        return model;
    }
    
    @RequestMapping(value="/category/save",method = RequestMethod.POST)
    public ModelAndView  saveCategory(SkillCategory category)
    {
        ModelAndView model = new ModelAndView("redirect:/skill/categoryList");
        skillCategoryService.updateSkillCategory(category);
        return  model;
    }
    
    @RequestMapping(value="/category/delete/{id}",method = RequestMethod.GET)
    public ModelAndView deleteCategory(@PathVariable(value = "id") int id)
    {
        ModelAndView model = new ModelAndView("redirect:/skill/categoryList");
        skillCategoryService.removeSkillCategory(id);
        return model;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addSkill()
    {
       ModelAndView model = new ModelAndView("/skill/skillAdd");
       Skill skill = new Skill();
       List<SkillCategory> categories = skillCategoryService.listAllSkillCategories();
       model.addObject("skillForm", skill);
       model.addObject("categories", categories);
       return model;
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public ModelAndView updateSkill(@PathVariable("id") int id)
    {
       ModelAndView model = new ModelAndView("/skill/skillAdd");
       Skill skill = skillService.getSkillById(id);
       List<SkillCategory> categories = skillCategoryService.listAllSkillCategories();
       model.addObject("skillForm", skill);
       model.addObject("categories", categories);
       return model;
    }
    
    
    @RequestMapping(value="/save", method = RequestMethod.POST)
    public ModelAndView saveSkill(@ModelAttribute("skillForm")Skill skill)
    {
        ModelAndView model = new ModelAndView("redirect:/skill/categoryList");
        skillService.updateSkill(skill);
        return model;
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteSkill(@PathVariable("id") int id)
    {
       ModelAndView model = new ModelAndView("redirect:/skill/categoryList");
       skillService.removeSkill(id);
       return model;
    }
    
//    public ModelAndView 
}
