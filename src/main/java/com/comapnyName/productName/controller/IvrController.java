package com.comapnyName.productName.controller;

import com.comapnyName.productName.model.IVR;
import com.comapnyName.productName.model.IVRinfo;
import com.comapnyName.productName.model.PBXIVRTree;
import com.comapnyName.productName.service.IvrInfoService;
import com.comapnyName.productName.service.IvrService;
import com.comapnyName.productName.util.service.IvrUtil;
import com.comapnyName.productName.service.PbxIvrTreeService;
import com.comapnyName.productName.service.QueueService;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dARINDA
 */
@Controller
@RequestMapping(value = "ivr")
public class IvrController
{

    @Autowired
    private IvrService ivrService;

    @Autowired
    private IvrInfoService ivrInfoService;

    @Autowired
    private PbxIvrTreeService pbxIvrTreeService;
    
    @Autowired 
    private QueueService queueService;
    
    @Autowired
    private IvrUtil ivrUtil;
    
    
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ModelAndView listIvr()
    {
        List<IVR> ivrList = ivrService.getAllIvrList();
        ModelAndView model = new ModelAndView();
        model.setViewName("ivr/list");
        model.addObject("ivrList", ivrList);
        return model;
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView addNewIvr()
    {
        ModelAndView model = new ModelAndView();
        model.setViewName("ivr/addIvr");
        IVR ivr = new IVR();
        model.addObject("ivrForm", ivr);
        return model;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ModelAndView saveNewIvr(IVR ivr)
    {
        ModelAndView model = new ModelAndView("redirect:/ivr/list");
        Date createdOn = new Date();
        ivr.setCreatedOn(createdOn);
        int ivrID = ivrService.insertIvrInDB(ivr);
        List<IVRinfo> ivrs = ivrUtil.createIvrList(ivrID);
        PBXIVRTree iVRTree = new PBXIVRTree();
        iVRTree.setIvrID(ivrID);
        iVRTree.setKeyPress(0);
        iVRTree.setName("ROOT");
        iVRTree.setNodeID(0);
        iVRTree.setNodeType("BRANCH");
        iVRTree.setPromptStatus(0);
        iVRTree.setQueueID(ivrID);
        pbxIvrTreeService.savePbxIvrTree(iVRTree);
        ivrInfoService.insertIvrInfolist(ivrs);
        queueService.saveFirstQueue(ivrID);
        return model;
    }

    @RequestMapping(value = "update/{id}", method = RequestMethod.GET)
    public ModelAndView updateIvr(@PathVariable(value = "id") int id, HttpServletRequest request)
    {
        ModelAndView model = new ModelAndView("/ivr/editIvr");
        IVR ivr = ivrService.getIvrByID(id);
        List<IVRinfo> infoList = ivrInfoService.getIvrInfoList(id);
        ivrUtil.addingIvrInfoInModel(model, infoList);
        model.addObject("ivrModel", ivr);
        model.addObject("ivrInfoModel", infoList);
        return model;
    }

    @RequestMapping
    public IVR getIvr(int ivrID)
    {
        IVR ivr = ivrService.getIvrByID(ivrID);
        return ivr;
    }

    
}
