/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.controller.rest;

import com.comapnyName.productName.model.IVRinfo;
import com.comapnyName.productName.service.IvrInfoService;
import com.comapnyName.productName.util.model.Message;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author dARINDA
 */
@CrossOrigin
@RestController
@RequestMapping(value = "ivr")
public class IvrRestController
{

    @Autowired
    private IvrInfoService ivrInfoService;

    @RequestMapping(value = "upload/music", method = RequestMethod.POST, produces =
    {
        "application/json", "application/xml"
    })
    public @ResponseBody
    Message updateMusic(@RequestParam("name") String name, @RequestParam("file") MultipartFile file)
    {
        Message message = new Message();

        if (!file.isEmpty())
        {
            try
            {
                byte[] bytes = file.getBytes();
                File dir = new File("D:\\Office Setup\\Office Projects\\upload" + File.separator);
                File serverFile = new File(dir.getAbsolutePath() + File.separator + name + ".jpg");
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
                message.setStatus("true");
                message.setMessage("File upload succesfully");
            } catch (Exception e)
            {
                message.setStatus("false");
                message.setMessage("File upload not succesfully " + e.getMessage());
            }
        } else
        {
            message.setStatus("false");
            message.setMessage("You failed to upload " + name + " because the file was empty.");
        }

        return message;
    }

    public @ResponseBody
    Message updateIvrInfo(@RequestParam("keyword") String keyword, @RequestParam("value") String value, @RequestParam("ivrID") int ivrID)
    {
        Message message = new Message();
        message.setStatus("true");
        message.setMessage("updated successfully");
        IVRinfo iVRinfo = new IVRinfo();
        iVRinfo.setKeyword(keyword);
        iVRinfo.setValue(value);
        iVRinfo.setIvrID(ivrID);
        ivrInfoService.updateIvrInfo(iVRinfo);
        return message;
    }

    public @ResponseBody
    Message removeIvrInfo(@RequestParam("keyword") String keyword, @RequestParam("value") String value, @RequestParam("ivrID") int ivrID)
    {
        Message message = new Message();
        message.setStatus("true");
        message.setMessage("removed successfully");
        IVRinfo iVRinfo = new IVRinfo();
        iVRinfo.setKeyword(keyword);
        iVRinfo.setValue(value);
        iVRinfo.setIvrID(ivrID);
        ivrInfoService.removeIvrInfo(keyword, value, ivrID);
        return message;
    }

}
