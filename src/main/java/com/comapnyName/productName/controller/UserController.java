package com.comapnyName.productName.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.comapnyName.productName.model.User;
import com.comapnyName.productName.service.UserService;

@Controller
//@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/user/all", method = RequestMethod.GET)
	public ModelAndView list()
	{
		ModelAndView modelAndView = new ModelAndView("user/list");
		System.out.println("use/list is being called");
		List<User> list = userService.listAllUsers();
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	@RequestMapping(value = "/user/update/{id}",method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") int id)
	{
		ModelAndView model = new ModelAndView("user/form");
		User user = userService.findUserById(id);
		model.addObject("userForm",user);
		return model;
	}	
	
	@RequestMapping(value = "/user/add",method = RequestMethod.GET)
	public ModelAndView add()
	{
		ModelAndView model = new ModelAndView("user/form");
		User user  = new User();
		model.addObject("userForm", user);
		return model;
	}
	
	@RequestMapping(value = "/user/save",method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("user/form") User user)
	{
		userService.saveOrUpdate(user);
		ModelAndView model = new ModelAndView("redirect:/user/form");
		return model;
	}
	
	
}
