package com.comapnyName.productName.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.comapnyName.productName.model.Login;
import com.comapnyName.productName.service.LoginService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class LoginController
{

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView loginPage()
    {
        ModelAndView model = new ModelAndView();
        Login login = new Login();
        model.addObject("loginForm", login);
        model.setViewName("index");
        return model;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView authenticate()
    {
        ModelAndView model = new ModelAndView();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Login login = new Login(username, password);
        boolean authenticated = loginService.authenticate(login);
        if (authenticated)
        {
            model.setViewName("redirect:/dashboard");
        } else
        {
            model.addObject("fail", "Invalid username or password");
            model.setViewName("/index");
        }
        return model;
    }
    
    @RequestMapping(value = "/dashboard",method = RequestMethod.GET)
    public ModelAndView dashboard()
    {
        ModelAndView model = new ModelAndView("dashboard");
        return model;
    }
}
