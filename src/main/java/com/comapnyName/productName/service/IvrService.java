/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service;

import com.comapnyName.productName.model.IVR;
import com.comapnyName.productName.model.IVRinfo;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface IvrService
{
    public int insertIvrInDB(IVR ivr);
    public boolean insertIvrConf(int id);
    
    
    public void removeIvrFromDB(int id);
    public boolean removeFromIvrConf(int id);
    
    
    public List<IVR> getAllIvrList();
    public IVR getIvrByID(int id);
    
}
