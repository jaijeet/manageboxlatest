package com.comapnyName.productName.service;

import java.util.List;

import com.comapnyName.productName.model.User;

public interface UserService {
	public List<User> listAllUsers();

	public void saveOrUpdate(User user);

	public User findUserById(int id);

	public void deleteUser(int id);
	
}
