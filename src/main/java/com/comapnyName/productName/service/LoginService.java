/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service;

import com.comapnyName.productName.model.Login;

/**
 *
 * @author jaijeet
 */
public interface LoginService
{
    boolean authenticate(Login login);
}
