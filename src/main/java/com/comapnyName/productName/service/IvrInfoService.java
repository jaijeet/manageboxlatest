/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service;

import com.comapnyName.productName.model.IVRinfo;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface IvrInfoService
{
    public void insertIvrInfolist(List<IVRinfo> list);
    public void removeIvrInfoList(int id);
    public List<IVRinfo> getIvrInfoList(int id);
    public IVRinfo getIvrinfoBykeyword(String keyword, int id);
    public void removeIvrInfoByKeyword(String keyword, int id);
    public void updateIvrInfo(IVRinfo iVRinfo);
    public void removeIvrInfo(String keyword, String value,int id);
}
