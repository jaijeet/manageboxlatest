/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service;

import com.comapnyName.productName.model.SkillCategory;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface SkillCategoryService
{
    List<SkillCategory> listAllSkillCategories();

    SkillCategory getSkillCategoryByID(int id);

    void addNewSkillCategory(SkillCategory category);

    void updateSkillCategory(SkillCategory category);
    
    void removeSkillCategory(int id);
}
