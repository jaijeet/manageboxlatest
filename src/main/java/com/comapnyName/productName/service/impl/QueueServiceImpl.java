/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.QueueDao;
import com.comapnyName.productName.model.Queue;
import com.comapnyName.productName.service.QueueInfoService;
import com.comapnyName.productName.service.QueueService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class QueueServiceImpl implements QueueService
{
    
    @Autowired
    private QueueDao queueDao;

    @Autowired
    private QueueInfoService infoService;
    
    @Override
    public void saveFirstQueue(int ivrID)
    {
        Queue queue = new Queue();
        queue.setId(ivrID);
        queue.setIvrID(ivrID);
        queue.setOnKey(11);
        queue.setName("Default");
        int qid = queueDao.saveQueue(queue);
        infoService.saveNewQueueInfo(qid);
    }
    
}
