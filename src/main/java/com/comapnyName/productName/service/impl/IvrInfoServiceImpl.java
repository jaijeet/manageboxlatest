/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.IvrInfoDao;
import com.comapnyName.productName.model.IVRinfo;
import com.comapnyName.productName.service.IvrInfoService;
import java.util.Iterator;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class IvrInfoServiceImpl implements IvrInfoService
{
    
    @Autowired
    private IvrInfoDao  ivrInfoDao;

    @Override
    public void insertIvrInfolist(List<IVRinfo> list)
    {
        Iterator<IVRinfo> iterator = list.iterator();
        while (iterator.hasNext())
        {
            IVRinfo ivrInfo = iterator.next();
            ivrInfoDao.saveIVR(ivrInfo);
        }
    }

    @Override
    public void removeIvrInfoList(int id)
    {
        ivrInfoDao.removeIvrInfoList(id);
    }

    @Override
    public List<IVRinfo> getIvrInfoList(int id)
    {
        return ivrInfoDao.getIvrInfoByIvrID(id);
    }

    @Override
    public IVRinfo getIvrinfoBykeyword(String keyword, int id)
    {
        return ivrInfoDao.getIvrInfoByID(keyword, id);
    }

    @Override
    public void removeIvrInfoByKeyword(String keyword, int id)
    {
        ivrInfoDao.deleteIvrInfoByName(keyword, id);
    }

    @Override
    public void updateIvrInfo(IVRinfo iVRinfo)
    {
        ivrInfoDao.updateIvrInfo(iVRinfo);
    }

    @Override
    public void removeIvrInfo(String keyword, String value, int id)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
