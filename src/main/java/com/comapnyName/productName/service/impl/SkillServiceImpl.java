/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.SkillDao;
import com.comapnyName.productName.model.Skill;
import com.comapnyName.productName.service.SkillService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class SkillServiceImpl implements SkillService
{
    
    @Autowired
    private SkillDao  skillDao;

    @Override
    public List<Skill> listAllSkills()
    {
        return skillDao.listAllSkills();
    }

    @Override
    public Skill getSkillById(int id)
    {
        return skillDao.getSkillById(id);
    }

    @Override
    public void createNewSkill(Skill skill)
    {
        skillDao.createNewSkill(skill);
    }

    @Override
    public void updateSkill(Skill skill)
    {
        skillDao.updateSkill(skill);
    }

    @Override
    public void removeSkill(int id)
    {
        skillDao.removeSkill(id);
    }
    
}
