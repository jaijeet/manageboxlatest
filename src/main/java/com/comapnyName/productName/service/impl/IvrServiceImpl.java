/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.IvrDao;
import com.comapnyName.productName.model.IVR;
import com.comapnyName.productName.service.IvrService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class IvrServiceImpl implements IvrService
{
    @Autowired
    private IvrDao ivrDao;
    
    @Override
    public int insertIvrInDB(IVR ivr)
    {
        return ivrDao.saveIVR(ivr);
    }

    @Override
    public boolean insertIvrConf(int id)
    {
        boolean result = false;
        
        return result;
    }

    @Override
    public void removeIvrFromDB(int id)
    {
        ivrDao.deleteIVR(id);
    }

    @Override
    public boolean removeFromIvrConf(int id)
    {
        boolean result = false;
        
        return result;
    }

    @Override
    public List<IVR> getAllIvrList()
    {
        return ivrDao.getAllIvrList();
    }

    @Override
    public IVR getIvrByID(int id)
    {
        return ivrDao.getIvrByID(id);
    }
    
}
