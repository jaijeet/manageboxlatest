/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.PbxIvrTreeDao;
import com.comapnyName.productName.model.PBXIVRTree;
import com.comapnyName.productName.service.PbxIvrTreeService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class PbxIvrTreeServiceImpl implements  PbxIvrTreeService
{

    @Autowired
    private PbxIvrTreeDao ivrTreeDao;
    
    
    @Override
    public List<PBXIVRTree> getPbxIvrTreeByIvrID(int ivrID)
    {
        return ivrTreeDao.getPbxIvrTreeByIvrID(ivrID);
    }

    @Override
    public PBXIVRTree getPbxIvrTreeById(int id)
    {
        return ivrTreeDao.getPbxIvrTreeById(id);
    }

    @Override
    public void savePbxIvrTree(PBXIVRTree iVRTree)
    {
        ivrTreeDao.savePbxIvrTree(iVRTree);
    }

    @Override
    public void deletePbxIvrTree(int id)
    {
        ivrTreeDao.deletePbxIvrTree(id);
    }

    @Override
    public void updatePbxIvrTree(PBXIVRTree ivrTree)
    {
        ivrTreeDao.updatePbxIvrTree(ivrTree);
    }
    
}
