/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.QueueInfoDao;
import com.comapnyName.productName.model.QueueInfo;
import com.comapnyName.productName.service.QueueInfoService;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class QueueInfoServiceImpl implements QueueInfoService
{

    @Autowired
    private QueueInfoDao dao;

    @Override
    public void saveNewQueueInfo(int qid)
    {
        
        List<QueueInfo> queueInfos = new ArrayList<>();
        QueueInfo info = new QueueInfo();
        info.setKeyword("forwardCallTo");
        info.setValue("VoiceMail");
        info.setQid(qid);
        queueInfos.add(info);
        
        info = new QueueInfo();
        info.setKeyword("forwardCallTo");
        info.setValue("VoiceMail");
        info.setQid(qid);
        queueInfos.add(info);
        
        info = new QueueInfo();
        info.setKeyword("ringMode");
        info.setValue("All");
        info.setQid(qid);
        queueInfos.add(info);
        
        info = new QueueInfo();
        info.setKeyword("predictAnnounceFrequency");
        info.setValue("30");
        info.setQid(qid);
        queueInfos.add(info);
        
        info = new QueueInfo();
        info.setKeyword("avgQueueLogOutTime");
        info.setValue("3600");
        info.setQid(qid);
        queueInfos.add(info);
        
        dao.saveQueueInfoList(queueInfos);       
    }

}
