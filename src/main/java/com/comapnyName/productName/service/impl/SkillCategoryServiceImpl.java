/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.SkillCategoryDoa;
import com.comapnyName.productName.model.SkillCategory;
import com.comapnyName.productName.service.SkillCategoryService;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
@Transactional
public class SkillCategoryServiceImpl implements SkillCategoryService
{

    @Autowired
    private SkillCategoryDoa skillCategoryDoa;
    
    @Override
    public List<SkillCategory> listAllSkillCategories()
    {
        return skillCategoryDoa.listAllSkillCategories();
    }

    @Override
    public SkillCategory getSkillCategoryByID(int id)
    {
        return skillCategoryDoa.getSkillCategoryByID(id);
    }

    @Override
    public void addNewSkillCategory(SkillCategory category)
    {
        skillCategoryDoa.addNewSkillCategory(category);
    }

    @Override
    public void updateSkillCategory(SkillCategory category)
    {
        skillCategoryDoa.updateSkillCategory(category);
    }

    @Override
    public void removeSkillCategory(int id)
    {
        skillCategoryDoa.removeSkillCategory(id);
    }
    
}
