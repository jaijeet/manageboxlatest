/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.service.impl;

import com.comapnyName.productName.doa.LoginDao;
import com.comapnyName.productName.model.Login;
import com.comapnyName.productName.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaijeet
 */
@Service
public class LoginServiceImpl implements LoginService
{

    @Autowired
    private LoginDao loginDao;

    @Override
    public boolean authenticate(Login login)
    {
       
        return loginDao.authenticateUser(login.getUsername(), login.getPassword());
    }

}
