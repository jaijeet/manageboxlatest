package com.comapnyName.productName.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comapnyName.productName.doa.UserDao;
import com.comapnyName.productName.model.User;
import com.comapnyName.productName.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	public List<User> listAllUsers() {
		// TODO Auto-generated method stub
		return userDao.listAllUsers();
	}

	public void saveOrUpdate(User user) {
		userDao.saveOrUpdate(user);
	}

	public User findUserById(int id) {
		return userDao.findUserById(id);
	}

	public void deleteUser(int id) {
		userDao.deleteUser(id);
	}

}
