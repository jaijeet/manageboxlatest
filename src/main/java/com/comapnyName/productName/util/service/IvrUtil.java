/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.util.service;

import com.comapnyName.productName.model.IVRinfo;
import java.util.List;
import java.util.Map;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dARINDA
 */
public interface IvrUtil
{
    public Map<String, String> convertIvrInfoListToMap(List<IVRinfo> list);
    public List<IVRinfo> createIvrList(int ivrID);
    public void addingIvrInfoInModel(ModelAndView model, List<IVRinfo> ivrs);
}
