/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.util.service.impl;

import com.comapnyName.productName.model.IVRinfo;
import com.comapnyName.productName.util.service.IvrUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author dARINDA
 */

@Service
public class IvrUtilImpl implements IvrUtil
{

    @Override
    public Map<String, String> convertIvrInfoListToMap(List<IVRinfo> list)
    {
        Map<String, String> map = new HashMap<>();
        if(list!=null && list.size()>0)
        {
            Iterator<IVRinfo> it = list.iterator();
            while (it.hasNext())
            {
                IVRinfo iVRinfo = it.next();
                map.put(iVRinfo.getKeyword(), iVRinfo.getValue());
            }
        }
        return map;
    }
    
    @Override
    public List<IVRinfo> createIvrList(int ivrID)
    {
        List<IVRinfo> list = new ArrayList<IVRinfo>();

        IVRinfo info = new IVRinfo();
        info.setIvrID(ivrID);
        info.setKeyword("forwardCallTo");
        info.setValue("VoiceMail");
        list.add(info);

        info = new IVRinfo();
        info.setIvrID(ivrID);
        info.setKeyword("IsInboundActive");
        info.setValue("false");
        list.add(info);

        info = new IVRinfo();
        info.setIvrID(ivrID);
        info.setKeyword("IsNightMode");
        info.setValue("false");
        list.add(info);

        info = new IVRinfo();
        info.setIvrID(ivrID);
        info.setKeyword("dialExtensionKey");
        info.setValue("false");
        list.add(info);

        info = new IVRinfo();
        info.setIvrID(ivrID);
        info.setKeyword("callFilterMode");
        info.setValue("false");
        list.add(info);

        info = new IVRinfo();
        info.setIvrID(ivrID);
        info.setKeyword("isEnabledExtension");
        info.setValue("false");
        list.add(info);

//        info = new IVRinfo();
//        info.setIvrID(ivrID);
//        info.setKeyword("IsInboundActive");
//        info.setValue("false");
//        list.add(info);
//
//        info = new IVRinfo();
//        info.setIvrID(ivrID);
//        info.setKeyword("IsInboundActive");
//        info.setValue("false");
//        list.add(info);
//
//        info = new IVRinfo();
//        info.setIvrID(ivrID);
//        info.setKeyword("IsInboundActive");
//        info.setValue("false");
//        list.add(info);
//
//        info = new IVRinfo();
//        info.setIvrID(ivrID);
//        info.setKeyword("IsInboundActive");
//        info.setValue("false");
//        list.add(info);
        return list;
    }
    
    @Override
    public void addingIvrInfoInModel(ModelAndView model,List<IVRinfo> ivrs)
    {
        if(ivrs!=null && ivrs.size()>0)
        {
            Iterator<IVRinfo> it = ivrs.iterator();
            while (it.hasNext())
            {
                IVRinfo info = it.next();
                model.addObject(info.getKeyword(), info.getValue());
            }
        }
    }
}
