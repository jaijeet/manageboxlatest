/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.util.model;

//import java.io.Serializable;

/**
 *
 * @author dARINDA
 */
public class Message //implements Serializable
{
//    private static final long serialVersionUID = -7788619177798333712L;
    private String status;
    private String message;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
