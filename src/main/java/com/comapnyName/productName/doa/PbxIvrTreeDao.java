/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa;

import com.comapnyName.productName.model.PBXIVRTree;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface PbxIvrTreeDao
{
    public List<PBXIVRTree> getPbxIvrTreeByIvrID(int ivrID);
    public PBXIVRTree getPbxIvrTreeById(int id);
    public void savePbxIvrTree(PBXIVRTree iVRTree);
    public void deletePbxIvrTree(int id);
    public void updatePbxIvrTree(PBXIVRTree ivrTree);
}
