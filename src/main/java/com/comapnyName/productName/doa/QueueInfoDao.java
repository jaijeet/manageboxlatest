/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa;

import com.comapnyName.productName.model.QueueInfo;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface QueueInfoDao
{
    public void saveQueueInfo(QueueInfo info);
    
    public void saveQueueInfoList(List<QueueInfo> queueInfos);
}
