/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.IvrInfoDao;
import com.comapnyName.productName.model.IVRinfo;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class IvrInfoDaoImpl implements IvrInfoDao
{

    @Autowired
    private SessionFactory factory;

    private Session getSession()
    {
        return factory.getCurrentSession();
    }

    @Override
    public List<IVRinfo> getAllIvrList()
    {
        List<IVRinfo> ivrInfoList = null;
        Criteria criteria = getSession().createCriteria(IVRinfo.class);
        ivrInfoList = criteria.list();
        return ivrInfoList;
    }

    @Override
    public int saveIVR(IVRinfo ivr)
    {
        getSession().save(ivr);
        return ivr.getId();
    }

    @Override
    public void deleteIvrInfoByID(int id)
    {
        IVRinfo  info = (IVRinfo)  getSession().get(IVRinfo.class, id);
        getSession().delete(info);
    }

    @Override
    public void deleteIvrInfoByName(String keyword, int id)
    {
        Criteria criteria = getSession().createCriteria(IVRinfo.class);
        criteria.add(Restrictions.eq("keyword", keyword));
        criteria.add(Restrictions.eq("id", id));
        IVRinfo info = (IVRinfo)criteria.uniqueResult();
        getSession().delete(info);
    }

    @Override
    public IVRinfo getIvrInfoByID(String keyword, int id)
    {
        IVRinfo info  = null;
        Criteria criteria = getSession().createCriteria(IVRinfo.class);
        criteria.add(Restrictions.eq("keyword", keyword));
        criteria.add(Restrictions.eq("id", id));
        info = (IVRinfo)criteria.uniqueResult();
        return info;
    }

    @Override
    public List<IVRinfo> getIvrInfoByIvrID(int ivrID)
    {
        List<IVRinfo> list = null;
        Criteria criteria = getSession().createCriteria(IVRinfo.class);
        criteria.add(Restrictions.eq("ivrID", ivrID));
        list = criteria.list();
        return list;
    }

    @Override
    public void removeIvrInfoList(int id)
    {
        Query query = getSession().createQuery("delete from IVRInfo where ivrID= :ivrID");
        query.setInteger("ivrID", id);
        query.executeUpdate();
    }

    @Override
    public void updateIvrInfo(IVRinfo iVRinfo)
    {
        getSession().update(iVRinfo);
    }

    @Override
    public void removeIvrInfo(String keyword, String value, int id)
    {
        Criteria criteria = getSession().createCriteria(IVRinfo.class);
        criteria.add(Restrictions.eq("keyword", keyword));
        criteria.add(Restrictions.eq("value", value));
        criteria.add(Restrictions.eq("ivrID", id));
        IVRinfo info = (IVRinfo)criteria.uniqueResult();
        getSession().delete(info);
    }
    
    

}
