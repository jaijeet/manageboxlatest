/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.SkillCategoryDoa;
import com.comapnyName.productName.model.SkillCategory;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class SkillCategoryDaoImpl implements SkillCategoryDoa
{

    @Autowired
    private SessionFactory factory;
    
    private Session getSession()
    {
        return factory.getCurrentSession();
    }
    
    @Override
    public List<SkillCategory> listAllSkillCategories()
    {
        Criteria criteria = getSession().createCriteria(SkillCategory.class);
        return (List<SkillCategory>)criteria.list();
    }

    @Override
    public SkillCategory getSkillCategoryByID(int id)
    {
        return (SkillCategory) getSession().get(SkillCategory.class, id);
    }

    @Override
    public void addNewSkillCategory(SkillCategory category)
    {
        getSession().save(category);
    }

    @Override
    public void updateSkillCategory(SkillCategory category)
    {
        getSession().saveOrUpdate(category);
    }

    @Override
    public void removeSkillCategory(int id)
    {
        SkillCategory category = (SkillCategory)getSession().get(SkillCategory.class, id);
        getSession().delete(category);
    }
    
}
