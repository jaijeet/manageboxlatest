/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.QueueInfoDao;
import com.comapnyName.productName.model.QueueInfo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class QueueInfoDaoImpl implements QueueInfoDao
{

    @Autowired
    private SessionFactory sessionFactory;
    
    private Session getSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    @Override
    public void saveQueueInfo(QueueInfo info)
    {
        getSession().save(info);
    }

    @Override
    public void saveQueueInfoList(List<QueueInfo> queueInfos)
    {
        for (QueueInfo queueInfo : queueInfos)
        {
            getSession().save(queueInfo);
        }
    }
    
    
}
