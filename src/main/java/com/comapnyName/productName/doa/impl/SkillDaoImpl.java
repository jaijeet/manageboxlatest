/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.SkillDao;
import com.comapnyName.productName.model.Skill;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class SkillDaoImpl implements SkillDao
{
    @Autowired
    private SessionFactory  factory;
    
    private Session getSession()
    {
        return  factory.getCurrentSession();
    }

    @Override
    public List<Skill> listAllSkills()
    {
        Criteria criteria = getSession().createCriteria(Skill.class);
        return (List<Skill>) criteria.list();
    }

    @Override
    public Skill getSkillById(int id)
    {
        return (Skill)getSession().get(Skill.class, id);
    }

    @Override
    public void createNewSkill(Skill skill)
    {
        getSession().save(skill);
    }

    @Override
    public void updateSkill(Skill skill)
    {
        getSession().saveOrUpdate(skill);
    }

    @Override
    public void removeSkill(int id)
    {
        Skill skill = (Skill) getSession().get(Skill.class, id);
        getSession().delete(skill);
    }
    
}
