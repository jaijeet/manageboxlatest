/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.PbxIvrTreeDao;
import com.comapnyName.productName.model.PBXIVRTree;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class PbxIvrTreeDoaImpl implements PbxIvrTreeDao
{
    @Autowired
    private SessionFactory factory;       
    
    private Session getSession()
    {
        return factory.getCurrentSession();
    }
    
    @Override
    public List<PBXIVRTree> getPbxIvrTreeByIvrID(int ivrID)
    {
        Criteria criteria = getSession().createCriteria(PBXIVRTree.class);
        criteria.add(Restrictions.eq("ivrID", ivrID));
        List<PBXIVRTree> ivrTreeList = criteria.list();
        return ivrTreeList;
    }

    @Override
    public PBXIVRTree getPbxIvrTreeById(int id)
    {
        return (PBXIVRTree) getSession().get(PBXIVRTree.class, id);
    }

    @Override
    public void savePbxIvrTree(PBXIVRTree iVRTree)
    {
        getSession().save(iVRTree);
    }

    @Override
    public void deletePbxIvrTree(int id)
    {
        PBXIVRTree iVRTree = (PBXIVRTree)getSession().get(PBXIVRTree.class, id);
        if(iVRTree!=null)
        {
            getSession().delete(iVRTree);
        }
    }

    @Override
    public void updatePbxIvrTree(PBXIVRTree ivrTree)
    {
        getSession().update(ivrTree);
    }
    
}
