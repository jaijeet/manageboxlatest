/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.LoginDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class LoginDoaImpl implements LoginDao
{

    @Autowired
    private SessionFactory  factory;
    
    private Session getSession()
    {
        return factory.getCurrentSession();
    }
    
    @Override
    public boolean authenticateUser(String username, String password)
    {
        /*
        * There should be hibernate code but now we are using simple hard code of credentials
         */
        if(username.equals("admin") && password.equals("admin"))
        {
            return true;
        }else{
            return false;
        }
        
    }
 
}
