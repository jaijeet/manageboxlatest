/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa.impl;

import com.comapnyName.productName.doa.IvrDao;
import com.comapnyName.productName.model.IVR;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaijeet
 */
@Repository
public class IvrDaoImpl implements IvrDao
{

    @Autowired
    private SessionFactory factory;

    private Session getSession()
    {
        return factory.getCurrentSession();
    }

    
    @Override
    public List<IVR> getAllIvrList()
    {
        List<IVR> ivrList = null;
        
        Criteria criteria = getSession().createCriteria(IVR.class);
        ivrList = criteria.list();
        return ivrList;
    }

    @Override
    public int saveIVR(IVR ivr)
    {
        getSession().save(ivr);
        return ivr.getId();
    }

    @Override
    public void deleteIVR(int id)
    {
        IVR ivr = (IVR)getSession().get(IVR.class, id);
        getSession().delete(ivr);
    }

    @Override
    public IVR getIvrByID(int id)
    {
        return (IVR) getSession().get(IVR.class, id);
    }

}
