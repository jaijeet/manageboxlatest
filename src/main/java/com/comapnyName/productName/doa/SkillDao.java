/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa;

import com.comapnyName.productName.model.Skill;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface SkillDao
{
    List<Skill> listAllSkills();
    
    Skill getSkillById(int id);
    
    void createNewSkill(Skill skill);
    
    void updateSkill(Skill skill);
    
    void removeSkill(int id);
}
