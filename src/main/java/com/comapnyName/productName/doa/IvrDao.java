/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa;

import com.comapnyName.productName.model.IVR;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface IvrDao
{

    public List<IVR> getAllIvrList();

    public int saveIVR(IVR ivr);

    public void deleteIVR(int id);

    public IVR getIvrByID(int id);
    
}
