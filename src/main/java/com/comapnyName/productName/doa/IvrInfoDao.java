/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comapnyName.productName.doa;

import com.comapnyName.productName.model.IVRinfo;
import java.util.List;

/**
 *
 * @author jaijeet
 */
public interface IvrInfoDao
{

    public List<IVRinfo> getAllIvrList();
    
    public List<IVRinfo> getIvrInfoByIvrID(int ivrID);

    public int saveIVR(IVRinfo ivr);

    public void deleteIvrInfoByID(int id);

    public void deleteIvrInfoByName(String keyword, int id);

    public IVRinfo getIvrInfoByID(String keyword, int id);

    public void removeIvrInfoList(int id);
    
    public void updateIvrInfo(IVRinfo iVRinfo);
    
    public void removeIvrInfo(String keyword, String value,int id);
}
